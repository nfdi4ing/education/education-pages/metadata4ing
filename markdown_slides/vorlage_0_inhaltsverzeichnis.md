## Inhaltsverzeichnis

1. [Objective of this guide](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/metadata4ing/html_slides/testumgebung.html#/3)
2. [How to model a Research Process and its Results with m4i?](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/metadata4ing/html_slides/testumgebung.html#/4)
3. [Additional resources and further reading](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/metadata4ing/html_slides/testumgebung.html#/5)

